$(function () {

  /* ****************************************
     reveal el on scroll
  **************************************** */ 
  var $window           = $(window),
      win_height_padded = $window.height() * 1.1,
      isTouch           = Modernizr.touch;
  if (isTouch) { $('.revealOnScroll').addClass('animated'); }
  function revealOnScroll() {
    var scrolled = $window.scrollTop() + $window.height(),
        win_height_padded = $window.height() * 1.1;
    $(".revealOnScroll:not(.animated)").each(function () {
      var $this     = $(this),
          offsetTop = $this.offset().top;
      if (scrolled> offsetTop) {
        if ($this.data('timeout')) {
          window.setTimeout(function(){
            $this.addClass('animated ' + $this.data('animation'));
          }, parseInt($this.data('timeout'),10));
        } else {
          $this.addClass('animated ' + $this.data('animation'));
        }
      }
    });
  }

  /* ****************************************
     current state nav
  **************************************** */ 
  function currentNav(){
    var navHeight;
    if($(window).width() > 760){
      navHeight = $(".controller").outerHeight();
    } else {
      navHeight = $(".controller").outerHeight();
    }
    var scrolled  = $window.scrollTop() + navHeight + 1,
        nav       = $(".nav li").find("a");

    $(".nav li").each(function(){
      var $this   = $(this).find("a").attr('href'),
          offsetTop = $($this).offset().top,
          offsetBot = offsetTop + $($this).outerHeight();
      if (scrolled> offsetTop) {
        $(this).addClass("current");
      } else {
        $(this).removeClass("current");
      }
      if (scrolled> offsetBot) {
        $(this).removeClass("current");
      }
    });
  }

	/* ****************************************
		Common form stuff
	**************************************** */	
	// validate form fields
	$("form.validate").validate();

	/* ****************************************
		Plugins
	**************************************** */	
  /* reveal on scroll */
  $window.on('scroll', revealOnScroll);
  revealOnScroll();

  /* currentNav */
  $window.on('scroll', currentNav);

  /* gallery */
	var instance = $('.chocolat-parent').Chocolat({
    zoomedPaddingY: 200
  }).data('chocolat');
	var stellarActivated = false;

	$(window).on('load resize', function(){
		react_to_window();
		if($(window).width() >= 1000){
			instance.api();
		} else {
			instance.destroy();
			$('.chocolat-parent a').click(function(e){
				e.preventDefault();
				window.open($(this).data("location"));
			});
		};

		if($(window).width() < 760){
			$("#bgvid").hide();
		} else {
			$("#bgvid").show();
		}
	});

  /* video banner */
  var video = $("video")[0];
  video.load();
  video.play();
  var video_length_round = 10;
  // Functions
  function afterLoad(){
    $("video")[0].pause();
    $("video")[0].currentTime = 0;
    $(".custom-preloader").delay(800).fadeOut();
    $("video")[0].play();
  }
  if(video.onprogress){
    console.log(Math.round(this.buffered.end(0) / this.duration));
    var buffered = Math.round(this.buffered.end(0) / this.duration);
    if(buffered == 1) {
      afterLoad();
      this.removeEventListener("progress", arguments.callee, false);
    }
  } else {
    afterLoad();
  };

  /* clear header on scroll */
  $(window).scroll(function() {    
    if($(window).width() > 760){
      var scroll = $(window).scrollTop();
      if (scroll >= 50) {
        $("body").addClass("darkHeader");
      } else {
        $("body").removeClass("darkHeader");
      }
    }
  });

  /* parallax */
	function react_to_window() {
    if ($(window).width() <= 1000) {
      if (stellarActivated == true) {
          $(window).data('plugin_stellar').destroy();
          stellarActivated = false;
      }
    } else {
      if (stellarActivated == false) {
        $.stellar({
    			horizontalScrolling: false,
    			verticalOffset: 0,
    			horizontalOffset: 0
        });
        
        $(window).data('plugin_stellar').init();
        stellarActivated = true;
      }
    }
  }

	/* ****************************************
		Nav
	**************************************** */	
	var sections = $('.page')
	, nav = $('.nav-holder, #section0')
	, nav_height = $(".controller").outerHeight();

	nav.find('a').on('click', function () {
		var $el = $(this)
		, id = $el.attr('href');
    if($(window).width() > 760){
  		$('html, body').animate({
  			scrollTop: $(id).offset().top - nav_height + 6
  		}, 500);
    } else {
      $('html, body').animate({
        scrollTop: $(id).offset().top - 54
      }, 500);
    }

    $(".nav li").removeClass("current");
    $el.parents("li").addClass("current");

		return false;
	});

	$("#logo").click(function(e){
		e.preventDefault();
		$('html, body').animate({
	        scrollTop: 0
	    }, 500);

	});

  /* ****************************************
    form submit
  **************************************** */ 
  // Get the form.
  var form = $('#ajax-contact');
  // Get the messages div.
  var formMessages = $('#form-messages');

  // Set up an event listener for the contact form.
	$(form).submit(function(event) {

    // Stop the browser from submitting the form.
    event.preventDefault();
    // Serialize the form data.
		var formData = $(form).serialize();
		// Submit the form using AJAX.
		$.ajax({
		    type: 'POST',
		    url: $(form).attr('action'),
		    data: formData
		}).done(function(response) {
		    // Make sure that the formMessages div has the 'success' class.
		    $(formMessages).removeClass('error');
		    $(formMessages).addClass('success');

		    // Set the message text.
		    $(formMessages).text(response).fadeIn(500);

		    // Clear the form.
		    $('#name').val('');
		    $('#email').val('');
		    $('#message').val('');
		}).fail(function(data) {
		    // Make sure that the formMessages div has the 'error' class.
		    $(formMessages).removeClass('success');
		    $(formMessages).addClass('error');

		    // Set the message text.
		    if (data.responseText !== '') {
		        $(formMessages).text(data.responseText);
		    } else {
		        $(formMessages).text('Oops! An error occured and your message could not be sent.').fadeIn(500);
		    }
		});
	});

}); /* end init.js */
